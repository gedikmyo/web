-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2019 at 11:59 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sat`
--

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `members` int(11) NOT NULL DEFAULT '0',
  `class_admin` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`class_id`, `section_id`, `name`, `members`, `class_admin`, `active`) VALUES
(1, 1, 'Görsel Programlama - 1', 0, '[1]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `faculty_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty_admin` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`faculty_id`, `name`, `faculty_admin`, `active`) VALUES
(1, '', '', 1),
(2, 'MYO', '[1]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_url` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` int(11) NOT NULL,
  `uploader` int(11) NOT NULL,
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_url`, `title`, `message`, `class_id`, `uploader`, `upload_date`, `active`) VALUES
(1, 'test', 'title', 'message', 1, 1, '2019-03-14 21:44:27', 1),
(2, 'test', 'title', 'message', 1, 1, '2019-03-14 21:44:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE `membership` (
  `id` int(11) NOT NULL,
  `membership_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `membership`
--

INSERT INTO `membership` (`id`, `membership_id`, `user_id`, `type`, `active`) VALUES
(1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `message_id` int(11) NOT NULL,
  `target_user` int(11) NOT NULL,
  `unread` tinyint(1) NOT NULL DEFAULT '0',
  `message` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`message_id`, `target_user`, `unread`, `message`, `send_date`, `active`) VALUES
(1, 1, 0, 'naber tirrek', '2019-03-14 16:09:48', 1),
(2, 1, 0, 'naber tirrek', '2019-03-14 16:13:12', 1),
(3, 1, 0, 'naber tirrek', '2019-03-14 16:13:42', 1),
(4, 1, 0, 'naber tirrek', '2019-03-14 16:13:54', 1),
(5, 1, 0, 'naber tirrek', '2019-03-14 16:14:09', 1),
(6, 1, 0, 'naber tirrek', '2019-03-14 16:14:34', 1),
(7, 1, 0, 'naber tirrek', '2019-03-14 16:15:32', 1),
(8, 1, 0, 'naber tirrek', '2019-03-14 16:15:58', 1),
(9, 1, 0, 'naber tirrek', '2019-03-14 16:16:01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `section_id` int(11) NOT NULL,
  `faculty_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section_admin` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`section_id`, `faculty_id`, `name`, `section_admin`, `active`) VALUES
(1, 2, 'Bilgisayar Programcılığı (ÖÖ)', '[1]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `token_id` int(11) NOT NULL,
  `token` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_key` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_ip` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_mail` varchar(96) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`token_id`, `token`, `token_key`, `token_date`, `user_id`, `user_ip`, `user_mail`, `active`) VALUES
(1, 'fsHlXQLq5cqxFrILgIXmi6a9WMgeIZcvlKuJ0Wtd3NNHP50wMKfMhmhIExSkK4ZZ23VdUrwy3FbesFusWKjlo8JwvjcMtrA7GLsUGbxkTrydagwwF4bJO0bCq8zrb56t', 'upIw9YCecdoTdHFLGNtCea20L1GNB9BWWUdDZt7j0tNGU5rcIJ8MLusvvZPotQ1w4hYbS21jVSe7mdr87IURptw1wrsKlqnYWNuvXQquYfTu26DjV45wvfkzKMvIhr8B', '2019-03-10 12:24:37', 1, '192.44.21.12', 'kalender@kalender.com', 1),
(2, 'BVHcafWGOUVTBBaz2mhtvf0ovdFl7omsdV1KwHu5cfm0TPlJMwdeTHYtzGSGYoxslfaJpKXuIL9qtLp0aXtSBgEYRJsR7dcs9aDTOcmsfM0Tszhr2Sso8FM3TCGUNinV', '8DOYq0FYGlQWa64x56tMUNJbTI4ujRcQ5qaVbDO7aqMpy4lWv6YuOWSybTcb40DWtnT5xent6TYQAjeIR5591eawsUIAA6B50eLOC9RndLw4jXih4tYWtur8queoEHQQ', '2019-03-10 12:25:45', 1, '192.44.21.12', 'kalender@kalender.com', 1),
(3, 'uaF6IFoNQRDjo8EtG0HuaLCj20XMxWyIPXPJgwPmX1JzSOHhHqNhIgLJNdmd7Y1DVTxv613ccnUrzhsRQlikUt8b7Kbn4XkNMFezILRSbW113Ozljft6WVXJgn2ikbWI', 'vDNRSL4S59sypJ6QRncJaG3HGHH5DiSMsKsRt3JENReJRmFO1jQ5xlrz9eCPBdML9ODorzTWEvthdR1W8zVCaeWKFdOtd5QH341PkvhlThc3Jt2fyMghyuHodaPEIN5I', '2019-03-10 12:26:00', 1, '192.44.21.12', 'kalender@kalender.com', 1),
(4, 'bdHMfuxsN8HPJhDWBIuwpzYfOxEvGneG9Tc5pcl4TNAoQNiPVfvrq4zntwMi96fOryQrWUMxK3Qj3nVQsnVhwOjx6Usgng94RibQE3aR1Pj0j94UgDaj92t7OpIcwbnp', 'bRrNhPxzphne2wlC7rcBXlbIqMbzBEBnyMODjXEDDOiT30GSNCDSaoyct7vC7cy5m2qnc5RMYRH1uiu18UrGlRIrNG4ji8ewL19KWiYT2f3wIbTUEIzvbCW2bafcatAa', '2019-03-10 12:30:54', 1, '192.44.21.12', 'kalender@kalender.com', 1),
(5, '6BHxFeIueS9ouzQgpBJ0e9P1RlkBhNWgYQyxkXRxFbsv8wwNfwTanEfHy3UKkB41X151tksdgNs0Po7Q0LoBDB1svV1rQcMISkTpJzX1Zf0vpaaTOG8AbfLiGuYpyQaT', 'duNyPCrhUWj3ZyvDGWn7F9DKLRh5Vs1ScRVjin16QYXqHyXPxaLK1VpDDpoVMwrT4WpUAoCtXR0HvRqeEQPovbF9ptfABZoZmCrBLBF13fA9fyCuwcXVlH8Dxjk38Ats', '2019-03-10 12:32:19', 1, '192.44.21.12', 'kalender@kalender.com', 1),
(6, 'xcmhtMCkkAOzuRhNcnOa55THp880GsliddVxwUIZqWTd6GsYwRD38tX78Famega3qxZA2cBsKcGKsIwKwSuw0TDxWcPXo08uoMrlJYRMvkRzcQ64fBOFN31Vuo7K196C', 'gr6XUvOE1qL07w43RI0rB93dnzx9kTPZ1XFSBVBiwUITP7DZO80EgVAcds8R7hMkrkilPtwAk9POwsceLSPlqvNqM9xdmLPi0nDgkTpDvi7RjJFgnbQXOBYWaMjzxxqK', '2019-03-10 12:33:18', 1, '192.44.21.12', 'kalender@kalender.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_prefix` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `profile_pic` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_confirm` tinyint(1) DEFAULT '0',
  `power` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `password`, `password_prefix`, `name`, `surname`, `gender`, `register_date`, `profile_pic`, `email_confirm`, `power`, `active`) VALUES
(1, 'kalender', 'kalender@kalender.com', '123456', '123', 'M.', 'Kalender', 1, '2019-03-09 10:07:39', '', 1, 99, 1);

-- --------------------------------------------------------

--
-- Table structure for table `work`
--

CREATE TABLE `work` (
  `work_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `work_title` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_message` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_timeout` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work`
--

INSERT INTO `work` (`work_id`, `class_id`, `teacher_id`, `work_title`, `work_message`, `work_timeout`, `send_date`, `active`) VALUES
(1, 1, 1, 'testwork', 'message work', '0000-00-00 00:00:00', '2019-03-14 22:23:23', 1),
(2, 1, 1, 'testwork', 'message work', '0000-00-00 00:00:00', '2019-03-14 22:24:13', 1),
(3, 1, 1, 'testwork', 'message work', '0000-00-00 00:00:00', '2019-03-14 22:24:22', 1),
(4, 1, 1, 'testwork', 'message work', '0000-00-00 00:00:00', '2019-03-14 22:24:35', 1),
(5, 1, 1, 'testwork', 'message work', '0000-00-00 00:00:00', '2019-03-14 22:24:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `work_file`
--

CREATE TABLE `work_file` (
  `work_file_id` int(11) NOT NULL,
  `work_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work_file`
--

INSERT INTO `work_file` (`work_file_id`, `work_id`, `file_id`, `active`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `work_receive`
--

CREATE TABLE `work_receive` (
  `work_receive_id` int(11) NOT NULL,
  `work_id` int(11) NOT NULL,
  `message` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work_receive`
--

INSERT INTO `work_receive` (`work_receive_id`, `work_id`, `message`, `user_id`, `send_date`, `active`) VALUES
(1, 1, 'testffas buyurun hocam', 1, '2019-03-14 22:48:10', 1),
(2, 1, 'testffas buyurun hocam', 1, '2019-03-14 22:49:11', 1),
(3, 1, 'testffas buyurun hocam', 1, '2019-03-14 22:49:41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `work_receive_file`
--

CREATE TABLE `work_receive_file` (
  `work_receive_file_id` int(11) NOT NULL,
  `work_receive_id` int(11) NOT NULL,
  `work_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work_receive_file`
--

INSERT INTO `work_receive_file` (`work_receive_file_id`, `work_receive_id`, `work_id`, `user_id`, `send_date`, `file_id`, `active`) VALUES
(1, 1, 1, 1, '2019-03-14 22:48:10', 1, 0),
(2, 1, 1, 1, '2019-03-14 22:48:10', 2, 0),
(3, 2, 1, 1, '2019-03-14 22:49:11', 1, 0),
(4, 2, 1, 1, '2019-03-14 22:49:11', 2, 0),
(5, 3, 1, 1, '2019-03-14 22:49:41', 1, 0),
(6, 3, 1, 1, '2019-03-14 22:49:41', 2, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`section_id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`token_id`),
  ADD UNIQUE KEY `token` (`token`),
  ADD UNIQUE KEY `token_key` (`token_key`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`work_id`);

--
-- Indexes for table `work_file`
--
ALTER TABLE `work_file`
  ADD PRIMARY KEY (`work_file_id`);

--
-- Indexes for table `work_receive`
--
ALTER TABLE `work_receive`
  ADD PRIMARY KEY (`work_receive_id`);

--
-- Indexes for table `work_receive_file`
--
ALTER TABLE `work_receive_file`
  ADD PRIMARY KEY (`work_receive_file_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `faculty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `membership`
--
ALTER TABLE `membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `token_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `work`
--
ALTER TABLE `work`
  MODIFY `work_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `work_file`
--
ALTER TABLE `work_file`
  MODIFY `work_file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `work_receive`
--
ALTER TABLE `work_receive`
  MODIFY `work_receive_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `work_receive_file`
--
ALTER TABLE `work_receive_file`
  MODIFY `work_receive_file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
