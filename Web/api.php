<?php
/**
 * Copyright (c) 2019.
 * PHP: 7.2.1
 * Bootstrap 4.3.1
 */

/**
 * IDE: IntelliJ IDEA.
 * Project: ProjeSAT
 * Owner: M. Kalender
 * Contact: muhammedkalender@protonmail.com
 * Date: 03-Mar-19
 * Time: 13:29
 */
//todo, /user/login gibi yolalr yap
//todo
include "lang/tr.php";

include "include/functions.php";

if(isset($_REQUEST["api"])) {
    $apiRequest = $_REQUEST["api"];
    if($apiRequest == "user_login") {
        echo json_encode(userLogin());
        exit();
    }
}