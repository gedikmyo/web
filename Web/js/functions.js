/*
 * IDE: IntelliJ IDEA
 * Project: ProjeSAT
 * Owner: M. Kalender
 * Contact: muhammedkalender@protonmail.com
 * Date: 03-03-2019
 * Time: 19:25
 */

var lang = {
    "check_null": '<?=l("check_null")?>',
    "check_type": '<?=l("check_type")?>',
    "check_short": '<?=l("check_short")?>',
    "check_long": '<?=l("check_long")?>'
};

//GetElementById
function g(paramName) {
    return this.getElementById(paramName);
}

function l(paramName) {
    return l(paramName, "", "");
}

function l(paramName, paramValue1) {
    return l(paramName, paramValue1, "");
}

function l(paramName, paramValue1, paramValue2) {
    if (paramName == "") {
        //todo
    }

    var result = lang[paramName];

    if (paramValue1 != "") {
        result = result.replace("[%PARAM1%]", paramValue1);
    }

    if (paramValue2 != "") {
        result = result.replace("[%PARAM2%]", paramValue2);
    }

    return result;
}

/*
    0 Object Name,
    1 Lang,
    2 Min
    3 Max
 */
function checkInput(paramInputs) {
    for (var i = 0; i < count(paramInputs); i++) {
        var input = paramInputs[i];
        var obj = g(input[0]);

        if (obj == null) {
            return [false, l("check_null", input[1])];
        }

        if (obj.value == null || obj.value == "") {
            return [false, l("check_null", input[1])];
        }

        if (input[2] > 0 && obj.value.length < input[2]) {
            return [false, l("check_short", input[1], input[2])];
        }

        if (input[3] > 0 && obj.value.length > input[3]) {
            return [false, l("check_long", input[1], input[3])];
        }
    }

    return [true];
}