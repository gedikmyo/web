<?php
/**
 * Copyright (c) 2019.
 * PHP: 7.2.1
 * Bootstrap 4.3.1
 */

/**
 * IDE: IntelliJ IDEA.
 * Project: ProjeSAT
 * Owner: M. Kalender
 * Contact: muhammedkalender@protonmail.com
 * Date: 03-Mar-19
 * Time: 01:55
 */

$arrLang = array(
    "homePage" => "Ana Sayfa",
    "homeWorks" => "Ödevler",
    "classes" => "Sınıflar",
    "check_error" => "[%PARAM1%] İle ilgili bir sorun var",
    "check_null" => "[%PARAM1%] Boş olamaz",
    "check_type" => "[%PARAM1%] Uygun formatta değil",
    "check_short" => "[%PARAM1%], [%PARAM2%] Harften kısa olamaz",
    "check_long" => "[%PARAM1%], [%PARAM2%] Harften uzun olamaz",
    "wrong_password" => "Kullanıcı adı veya şifre yanlış",
    "already_username" => "Kullanıcı adı kullanılıyor",
    "already_email" => "Eposta adresi kullanılıyor",
    "register_success" => "Kullanıcı başarıyla kayıt edildi",
    "register_failure" => "Kullanıcı kayıt edilemedi",
    "auth_problem" => "Bu işlemi yapmak için yetkiniz yok",
    "problem_register" => "Kayıt sırasında hata oluştu",
    "failure_send_message" => "Kullanıcılara mesaj gönderilmedi",
    "failure_send_message_semi" => "Kullanıcılardan bazılarına mesaj gönderilemedi",
    "success_send_message" => "Kullanıcılara mesaj gönderildi",
    "success_insert_work" => "Ödev başarıyla eklendi",
    "failure_insert_work" => "Ödev eklenirken bir sorunla karşılaşıldı",
    "failure_insert_work_attachment" => "Ekler yüklenirken sorunla karşılaşıldı",
    "failure_insert_work_attachment_semi" => "Bazı ekler yüklenirken sorunla karşılaşıldı",
    "failure_work_receive_file" => "Ekler yüklenirken sorunla karşılaşıldı",
    "failure_work_receive_file_semi" => "Bazı ekler yüklenirken sorunla karşılaşıldı",
    "success_work_receive" => "Ödev başarıyla yüklendi",
    "failure_work_receive" => "Ödev yüklenirken bir sorunla karşılaşıldı"
);

//Lang
function l($ParamLang, $ParamValue1 = "", $ParamValue2 = ""){
    global $arrLang;

    if(isset($arrLang[$ParamLang])) {
        $result = $arrLang[$ParamLang];

        if($ParamValue1 != "") {
            $result = str_replace("[%PARAM1%]", $ParamValue1, $result);
        }

        if($ParamValue2 != "") {
            $result = str_replace("[%PARAM2%]", $ParamValue2, $result);
        }

        return $result;
    }

    return $ParamLang;
}

//Print
function p($ParamLang){
    global $arrLang;

    if(isset($arrLang[$ParamLang])) {
        echo $arrLang[$ParamLang];
    }

    echo $ParamLang;
}