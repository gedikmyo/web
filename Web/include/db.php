<?php
/**
 * Copyright (c) 2019.
 * PHP: 7.2.1
 * Bootstrap 4.3.1
 */

/**
 * IDE: IntelliJ IDEA.
 * Project: ProjeSAT
 * Owner: M. Kalender
 * Contact: muhammedkalender@protonmail.com
 * Date: 03-Mar-19
 * Time: 13:17
 */

$host = 'localhost';
$db_name = 'sat';
$user = 'root';
$pass = '';
$charset = 'utf8mb4';

$db;

$dsn = "mysql:host=$host;dbname=$db_name;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];
try {
    $db = new PDO($dsn, $user, $pass, $options);
} catch (PDOException $e) {
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}