<?php
/**
 * IDE: IntelliJ IDEA
 * Project: ProjeSAT
 * Owner: M. Kalender
 * Contact: muhammedkalender@protonmail.com
 * Date: 03-03-2019
 * Time: 22:23
 */

include_once "include/functions.php";
include_once "include/db.php";
include_once "lang/tr.php"; //todo

$user = new User();

class InputMethod
{
    const GET = 0;
    const POST = 1;
    const REQUEST = 2; //todo
    const SESSION = 3;
}

class InputType
{
    const Int = 0;
    const Float = 1;
    const String = 2; //0-9, a-Z, . , _ - + - / *
    const Text = 3; //Replace all
    const URL = 4;
    const EMAIL = 5;
}

class InputObject
{
    public $isCorrect = 1;
    public $name = "";
    public $langName = "";
    public $minLength = 0;
    public $maxLength = 0;
    public $method = 0;
    public $isNullable = false;
    public $inputType;

    /*
     * Dil kısmı boş bırakılırsa dil dosyasında [var.] şeklinde prefixle aratır
     */
    public function __construct($paramName, $paramLangName, int $paramMinLength, int $paramMaxLength, int $paramInputType, int $paramMethod = InputMethod::POST, bool $paramNullable = false){
        if($paramName == "" || $paramName == null) {
            $this->isCorrect = 0;
        }

        $this->name = $paramName;
        $this->langName = $paramLangName;
        $this->minLength = $paramMinLength;
        $this->maxLength = $paramMaxLength;
        $this->method = $paramMethod;
        $this->isNullable = $paramNullable;
        $this->inputType = $paramInputType;

        if($this->langName == null || $this->langName == "") {
            $this->langName = "var" . $paramName;
        }
    }
}

class Perm
{
    const Admin = 99;
    const TechnicalUser = 20; //Junior admins
    const AdvancedUser = 10; //Teachers
    const User = 1; //Students
    const Visitor = 0; //Not logged
}

class AuthorizationType
{
    const OR_UPPER_LESS_POWER = 1; //Bu yetkiden yüksek, ve kendisinin puanından düşük
    const OR_UPPER = 2;

    const ONLY_HIMSELF = 0;
}

//todo inner class =
class Authorization
{
    const INSERT_USER = [Perm::AdvancedUser, AuthorizationType::OR_UPPER_LESS_POWER];
    const UPDATE_USER = [Perm::TechnicalUser, AuthorizationType::OR_UPPER_LESS_POWER];
    const DELETE_USER = [Perm::AdvancedUser, AuthorizationType::OR_UPPER_LESS_POWER];
}

class User
{
    const NotSet = 0, Male = 1, Female = 2;

    public $id = 0;
    public $username;
    public $email;
    public $password; //For register
    public $passwordPrefix;
    public $name;
    public $surname;
    public $registerDate;
    public $power = Perm::User;
    public $profilePicture = ""; //Default ""
    public $gender = User::NotSet;
    public $ip;
    public $token;
    public $token_key;

    public function __construct(){
        //todo ip, lang

        if($this->CheckLogin()[0]) {
            $this->Get(Session::Get("user_id"));
        }
    }

    public function Get($userId){
        if(is_int($userId)) {
            $selectUser = DB::Select("SELECT * FROM user WHERE user_id = " . $userId);

            if($selectUser[0]) {
                $this->Fill($selectUser[1]);
            }
        }
    }

    public function Load(){
        //todo, session bilgilerini çekip ilgili kısımları doldur

        if(power == Perm::Visitor) {
            $this->username = "Visitor";
        }
    }

    //todo lang

    public function Fill($paramFetch){
        $this->id = $paramFetch["user_id"];
        $this->username = $paramFetch["username"];
        $this->email = $paramFetch["email"];
        $this->name = $paramFetch["name"];
        $this->surname = $paramFetch["surname"];
        $this->registerDate = $paramFetch["register_date"];
        $this->power = $paramFetch["power"];
        $this->profilePicture = $paramFetch["profile_pic"];
        $this->gender = $paramFetch["gender"];

        $this->token = Session::Get("token");
        $this->token_key = Session::Get("token_key");
    }

    //Default POST Method, For register
    public function Check(){
        //todo lang
        $checkResult = Text::Inputs([
            new InputObject("username", "", 3, 32, InputType::String),
            new InputObject("email", "", 3, 64, InputType::EMAIL),
            new InputObject("name", "", 3, 24, InputType::String),
            new InputObject("surname", "", 3, 24, InputType::String),
            new InputObject("password", "", 6, 64, InputType::String),
            new InputObject("gender", "", 1, 2, InputType::Int)
        ]);

        if($checkResult[0] == false) {
            return array($checkResult, "");
        }

        $this->password = "test";//todo
        $this->passwordPrefix = "asdas"; //todo
        //todo email check
        //todo username check
        //todo password check

        return true;//todo
    }

    /*
     * Secondary Power => Diğer kullanıcıyla etkileşim
     */
    public function Auth($paramAuthorization, $paramAuthorizationType, $paramSecondaryPower = 0){
        if($this->power < $paramAuthorization) {
            return false;
        }

        if($paramSecondaryPower != 0) {
            if($paramAuthorizationType == AuthorizationType::OR_UPPER_LESS_POWER) {
                if($this->power < $paramSecondaryPower) {
                    return false;
                }
            }

            if($paramAuthorizationType == AuthorizationType::OR_UPPER) {
                return true;
            }

            if($paramAuthorizationType == AuthorizationType::ONLY_HIMSELF) {
                return false;
            }
        }

        return true;
    }

    public function Password($paramRawPassword, $paramPrefix){
        //todo
    }

    /*
     * [POST]
     * 0 = Result
     * 1 = Error Message / User İd
     * 2 = Email
     * 3 = Token
     * 4 = Token Keyt
     */
    function Login(){
        $checkResult = Text::Inputs([
            new InputObject("user", "login_user", 3, 128, InputType::String),
            new InputObject("password", "login_password", 6, 64, InputType::String)
        ]);

        if($checkResult[0] == false) {
            return $checkResult;
        }

        $token = "";
        $token_key = "";

        global $db;

        $queryLogin = "SELECT user_id, email, profile_pic FROM user WHERE (email = :email OR username = :username) AND password = :password";

        $conn = $db->prepare($queryLogin);
        $conn->bindParam(":username", $_POST["user"], PDO::PARAM_STR);
        $conn->bindParam(":email", $_POST["user"], PDO::PARAM_STR);
        $conn->bindParam(":password", $_POST["password"], PDO::PARAM_STR);

        if($conn->execute()) {
            //todo
        } else {

        }

        $resultLogin = $conn->fetch(PDO::FETCH_ASSOC);

        if($conn->rowCount() == 0) {
            return array(false, l("wrong_password"));
        }

        do {
            $token = Text::Token();
            $token_key = Text::Token();

            $queryInsertToken = 'INSERT INTO token (token, token_key, user_id, user_ip, user_mail) VALUES 
                    ("' . $token . '", "' . $token_key . '", ' . $resultLogin["user_id"] . ', "' . GetIP() . '", "' . $resultLogin["email"] . '")';

            if($db->prepare($queryInsertToken)->execute()) {
                break;
            }

            //todo aynı olduğundanmı, harcii hatadanmı
        } while (true);

//todo

        DB::Execute("UPDATE token SET active = 0 WHERE user_id = " . $resultLogin["user_id"]); //todo cihaz seçimni olsun, mobildeki otruum dursun felan
        //todo
        Session::Set("token", $token);
        Session::Set("token_key", $token_key);
        Session::Set("user_id", $resultLogin["user_id"]);

        return array(1, $resultLogin["user_id"], $resultLogin["email"], $token, $token_key);
    }

    /*
     * 0 = Result
     * 1 = Message
     */
    function CheckLogin($paramUserId = "", $paramToken = "", $paramTokenKey = ""){
        //Parametre boşsa sessiondan yükleme
        if($paramUserId == "") {
            $paramUserId = Session::Get("user_id");
            $paramToken = Session::Get("token");
            $paramTokenKey = Session::Get("token_key");
        } else {
            Session::Set("user_id", $paramUserId);
            Session::Set("token", $paramToken);
            Session::Set("token_key", $paramTokenKey);
        }
        //todo lang
        $checkResult = Text::Inputs([
            new InputObject("user_id", "user_id", 1, 128, InputType::Int, InputMethod::SESSION),
            new InputObject("token", "token", 128, 128, InputType::String, InputMethod::SESSION),
            new InputObject("token_key", "token_key", 128, 128, InputType::String, InputMethod::SESSION)
        ]);

        if($checkResult[0] == false) {
            return array($checkResult, "");
        }

        global $db;

        $queryCheckLogin = $db->prepare("SELECT * FROM token WHERE token = :token AND token_key = :token_key AND user_id = :user_id");
        $queryCheckLogin->bindParam(":token", $paramToken, PDO::PARAM_STR);
        $queryCheckLogin->bindParam(":token_key", $paramTokenKey, PDO::PARAM_STR);
        $queryCheckLogin->bindParam(":user_id", $paramUserId, PDO::PARAM_INT);

        if(!$queryCheckLogin->execute()) {
            //todo
        }

        if($queryCheckLogin->rowCount() == 0) {
            //todo
            return [false, "un_logged"];
        }

        $resultCheckLogin = $queryCheckLogin->fetch(PDO::FETCH_ASSOC);

        if($resultCheckLogin["active"] == 0) {
            return array(false, "timeout"); //todo
        }
        //todo loginde geri kalnalrı false yap

        //todo timeout =?

        return array(true, "ok"); //todo
    }

    /*
     * Parametre içindeki kullanıcı kaydeder
     */
    function Register(User $paramUser = null){
        try {
            //todo
            if($paramUser == null) {
                $paramUser = this;
            } else {
                //Admin üye kaydediyor
                if($this->Auth(Authorization::INSERT_USER, AuthorizationType::OR_UPPER_LESS_POWER, $paramUser->power) == false) {
                    return [false, "auth_problem"];
                }
            }

            if(($checkResult = $paramUser->Check()) == false) {
                return $checkResult;
            }

            if(DB::Available("SELECT user_id FROM user WHERE email = '" . $this->email . "'")) {
                return [false, l("already_email")];
            }

            if(DB::Available("SELECT user_id FROM user WHERE username = '" . $this->username . "'")) {
                return [false, l("already_username")];
            }

            global $db;

            $queryInsertUser = $db->prepare("INSERT INTO user (username, email, password, password_prefix, name, surname, gender, profile_pic, power) VALUES (:username, :email, :password, :password_prefix, :name, :surname, :gender, :profile_pic, :power)");
            $queryInsertUser->bindParam(":username", $paramUser->username, PDO::PARAM_STR);
            $queryInsertUser->bindParam(":email", $paramUser->email, PDO::PARAM_STR);
            $queryInsertUser->bindParam(":password", $paramUser->password, PDO::PARAM_STR);
            $queryInsertUser->bindParam(":password_prefix", $paramUser->passwordPrefix, PDO::PARAM_STR);
            $queryInsertUser->bindParam(":name", $paramUser->name, PDO::PARAM_STR);
            $queryInsertUser->bindParam(":surname", $paramUser->surname, PDO::PARAM_STR);
            $queryInsertUser->bindParam(":gender", $paramUser->gender, PDO::PARAM_INT);
            $queryInsertUser->bindParam(":profile_pic", $paramUser->profilePicture, PDO::PARAM_STR);
            $queryInsertUser->bindParam(":power", $paramUser->power, PDO::PARAM_INT);

            if($queryInsertUser->execute()) {
                //todo

                return [true, "register_success"];
            } else {
                return [false, "register_failure"];
            }
        } catch (Exception $e) {
            Log::Error("register", $e);
            return [false, $e->getMessage()];
        }
    }

    function GetIP(){
        try {
            $ip = null;

            if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            return $ip;
        } catch (Exception $e) {
            Log::Error("ip", $e);
            return "ERR";
        }
    }
}

class Membership
{
    const CLASS_MEMBER = 1; // xin Üyesi
    const ADMIN_MEMBER = 2; // xin Admini

    public static function CheckMember($pUserId, $pMembershipId, $pMembershipType = Membership::CLASS_MEMBER){
        //todo auth
        return DB::Available("SELECT id FROM membership WHERE user_id = " . intval($pUserId) . " AND membership_id = " . intval($pMembershipId) . " AND type=" . intval($pMembershipType) . " AND active = 1");
    }

    public static function Members($pMembershipId, $pMembershipType = Membership::CLASS_MEMBER){
        //todo auth
        return DB::Select("SELECT user_id FROM membership WHERE membership_id = " . intval($pMembershipId) . " AND type = " . intval($pMembershipType) . " AND active = 1");
    }
}

//todo auth, check
class Message
{
    //Target is array
    /*
     * Hata durumunda, hata alınan kullanıcıları geri döndürür
     */
    //todo SQL injection olabilir, decode da halletmeye çalış yada check
    //todo sender ?
    public static function Send(array $pTarget, $pMessage){
        //todo check message

        $result = $pTarget;
        $errorCount = 0;

        for ($i = 0; $i < count($pTarget); $i++) {
            $result[$i] = DB::Execute(" INSERT INTO message (target_user, message) VALUES (" . $pTarget[$i] . ", '" . $pMessage . "')");

            if($result[$i] == false) {
                $errorCount++;
            }
        }

        if($errorCount == count($pTarget)) {
            return [false, l("failure_send_message"), $pTarget];
        }

        $errorUser = array();

        if($errorCount > 0) {
            //todo
            for ($i = 0; $i < count($pTarget); $i++) {
                if($result[$i] == false) {
                    array_push($errorUser, $pTarget[$i]);
                }

                return [false, l("failure_send_message_semi"), $errorUser];
            }
        } else {
            return [true, l("success_send_message")];
        }
    }

    //todo Pagintion
    public static function Receive($pUserId, $pUnread = 0, $pTimestamp = 0, $pActive = 1){
        //todo perm auth
        return DB::Select("SELECT * FROM message WHERE target_user = " . $pUserId . " AND unread = " . $pUnread . " AND send_date >= " . $pTimestamp . " AND active = " . $pActive);
    }

    public static function MarkRead($pMessageId){
        return DB::Execute("UPDATE message SET unread = 1 WHERE massage_id = " . intval($pMessageId));
    }

    public static function MarkUnRead($pMessageId){
        return DB::Execute("UPDATE message SET unread = 0 WHERE massage_id = " . intval($pMessageId));
    }

    public static function Delete($pMessageId){
        return DB::Execute("UPDATE message SET active = 0 WHERE massage_id = " . intval($pMessageId));
    }

}

class Log
{
    public static function Error($paramName, $paramMessage){
        if(get_class($paramMessage) == Exception::class) {

        }
        //todo
    }
}

class Cache
{
    const CACHE_TIME = 43200; //12 Hours

    public function Valid(User $paramUser, $paramSuffix){
        $fileURL = $this->Encrypt($paramUser->power . $paramUser->email . $paramSuffix);

        return $this->CheckFile($fileURL);
    }

    public function Encrypt($paramRaw){
        //todo

        return sha1($paramRaw);
    }

    public function CheckFile($paramURL){
        //todo

        $paramURL = "cache/" . $paramURL . ".html";

        if(file_exists($paramURL)) {
            if(time() < filemtime($paramURL) + Cache::CACHE_TIME) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }
}

class DB
{
    public static function Execute($paramQuery){
        global $db;
        return $db->prepare($paramQuery)->execute();
    }

    public static function ExecuteId($paramQuery){
        global $db;
        return [$db->prepare($paramQuery)->execute(), $db->lastInsertId()];
    }

    public static function Select($paramQuery){
        try {
            global $db;
            $conn = $db->prepare($paramQuery);

            if($conn->execute() == false) {
                return array(false, $conn->errorInfo());
            }

            return [true, $conn->fetchAll(PDO::FETCH_ASSOC)];
        } catch (Exception $e) {
            return [false, $e];
        }
    }

    /*
     * Koşul sağlanırsa false, sağlanmazsa true döner
     */
    public static function Available($paramQuery){
        return self::Select($paramQuery)[1] == null ? false : true;
    }
}

class Session
{

    public static function Delete($paramName){
        //https://stackoverflow.com/a/18542272
        if(session_status() == PHP_SESSION_NONE) {
            return false;
        }

        unset($_SESSION[$paramName]);

        return true;
    }

    public static function Get($paramName){
        //https://stackoverflow.com/a/18542272
        if(session_status() == PHP_SESSION_NONE) {
            return "";
        }

        return isset($_SESSION[$paramName]) ? $_SESSION[$paramName] : "";
    }

    public static function Set($paramName, $paramValue){
        //https://stackoverflow.com/a/18542272
        if(session_status() == PHP_SESSION_NONE) {
            return false;
        }

        $_SESSION[$paramName] = $paramValue;

        return true;
    }
}

class Text
{
    public static function Token($paramLength = 128){
        $list = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $listLength = strlen($list);

        $token = "";

        for ($i = 0; $i < $paramLength; $i++) {
            $randomIndex = rand(0, $listLength - 1); //todo -1
            $token .= $list[$randomIndex];
        }

        return $token;
    }

    public static function Encode($rawText){
//todo
    }

    public static function Decode($rawText){
//todo
    }

    public static function Inputs($arrInputs){
        foreach ($arrInputs as $input) {
            if(!$input->isCorrect) {
                return array(0, l("check_error", $input->langName));
            }

            $_checkMethod = false;
            $_var = "";
            switch ($input->method) {
                case InputMethod::GET:
                    $_checkMethod = isset($_GET[$input->name]);

                    if($_checkMethod) {
                        $_var = $_GET[$input->name];
                    }
                    break;
                case InputMethod::POST:
                    $_checkMethod = isset($_POST[$input->name]);

                    if($_checkMethod) {
                        $_var = $_POST[$input->name];
                    }
                    break;
                case InputMethod::REQUEST:
                    $_checkMethod = isset($_REQUEST[$input->name]);

                    if($_checkMethod) {
                        $_var = $_REQUEST[$input->name];
                    }
                    break;
                case InputMethod::SESSION:
                    $_checkMethod = Session::Get($input->name) == "" ? false : true;

                    if($_checkMethod) {
                        $_var = Session::Get($input->name);
                    }
                    break;
                default:
                    $_checkMethod = 0;
            }

            if(!$_checkMethod && !$input->isNullable || (!$input->isNullable && $_var == "")) {
                return array(0, l("check_null", $input->langName));
            }

            if($input->minLength > 0) {
                if(strlen($_var) < $input->minLength) {
                    return array(0, l("check_short", $input->langName, $input->minLength));
                }
            }

            if($input->maxLength > 0) {
                if(strlen($_var) > $input->maxLength) {
                    return array(0, l("check_long", $input->langName, $input->maxLength));
                }
            }

            $_checkInputType = false;

            switch ($input->inputType) {
                case InputType::Int:
                    $_checkInputType = filter_var($_var, FILTER_VALIDATE_INT);
                    break;
                case InputType::Float:
                    $_checkInputType = filter_var($_var, FILTER_VALIDATE_FLOAT);
                    break;
                case InputType::EMAIL:
                    $_checkInputType = filter_var($_var, FILTER_VALIDATE_EMAIL);
                    break;
                case InputType::String:
                    //todo
                    $_checkInputType = true;
                    break;
                case InputType::Text:
                    //todo;
                    $_checkInputType = true;
                    break;
                case InputType::URL:
                    $_checkInputType = filter_var($_var, FILTER_VALIDATE_URL);
                    break;
                default:
                    $_checkInputType = false;
            }

            if(!$_checkInputType) {
                return array(0, l("check_type", $input->langName));
            }
        }

        return array(1);
    }
}

//todo auth, result, check
class Attachment
{
    //todo active 0 sa ?
    public static function File($fileId){
        return DB::Select("SELECT * FROM file WHERE file_id = {$fileId}");
    }

    public static function Files($pClassId, $pActive = 1, $pUploader = -1){
        return DB::Select("SELECT * FROM file WHERE class_id ={$pClassId} AND active = {$pActive}" . ($pUploader == -1 ? "" : " AND uploader = " . $pUploader));
    }

//todo parantezi diğerlerinede ekle {$}
    public static function Add($pFileUrl, $pTitle, $pMessage, $pClassId, $pUploaderId){
        return DB::Execute("INSERT INTO file (file_url, title, message, class_id, uploader) VALUES ('{$pFileUrl}', '{$pTitle}', '{$pMessage}', {$pClassId}, {$pUploaderId})");
    }
}

//todo auth, result, check
class Work
{
    //todo Attachments => Files, ayrı db de tutulacak, parametede sadece id listesi olacak
    //todo message gönder
    public static function Add($pClassId, $pTeacherId, $pWorkTitle, $pWorkMessage, $pWorkTimeOut, array $pWorkAttachments = array()){
        $resultInsertWork = DB::ExecuteId("INSERT INTO work (class_id, teacher_id, work_title, work_message, work_timeout) VALUES ({$pClassId}, {$pTeacherId}, '{$pWorkTitle}', '{$pWorkMessage}', {$pWorkTimeOut})");

        if($resultInsertWork[0] == false) {
            return [false, l("failure_insert_work_")];
        }

        $resultWorkAttachments = $pWorkAttachments;
        $resultFailureCount = 0;

        for ($i = 0; $i < count($pWorkAttachments); $i++) {
            $resultWorkAttachments = DB::Execute("INSERT INTO work_file(work_id, file_id) VALUES ({$pWorkAttachments[$i]}, {$pWorkAttachments[$i]})");
        }

        if($resultFailureCount == count($pWorkAttachments)) {
            return [false, l("failure_insert_work_attachment")];
        }

        if($resultFailureCount == 0) {
            return [true, l("success_insert_work")];
        } else {
            $failureAttachmentList = array($resultFailureCount);

            $failureIndex = 0;

            for ($i = 0; $i < count($pWorkAttachments); $i++) {
                if($resultWorkAttachments{$i} == false) {
                    $failureAttachmentList{$failureIndex++} = $pWorkAttachments[$i];
                }
            }

            return [false, l("failure_insert_work_attachment"), $failureAttachmentList];
        }
    }

    //Sınıfla aynı isimde olmasın diye
    //todo sorguya bi göz at
    public static function GetWork($pWorkId){
        return DB::Select("SELECT * FROM work INNER JOIN work_file USING(work_id) WHERE work_id = {$pWorkId}");
    }

    //todo  file 0 sa == hata verebilir, diğerlerinide kontro let todo//
    //Dosyalar önden upload edilmiş olmalı ?
    public static function AddReceive($pWorkId, $pMessage, $pUserId, array $pReceiveFiles = array()){
        $resultReceive = DB::ExecuteId("INSERT INTO work_receive (work_id, message, user_id) VALUES ({$pWorkId}, '{$pMessage}', {$pUserId})");

        if($resultReceive[0] == false) {
            return [false, l("failure_work_receive")];
        }

        $receiveId = $resultReceive[1];

        $failureCount = 0;
        $resultReceiveFile = $pReceiveFiles;
        $failureList = array(count($pReceiveFiles));

        $receiveIndex = 0;

        for ($i = 0; $i < count($pReceiveFiles); $i++) {
            $resultReceiveFile[$i] = DB::Execute("INSERT INTO work_receive_file(work_receive_id, work_id, user_id, file_id) VALUES ({$receiveId}, {$pWorkId}, {$pUserId}, {$pReceiveFiles[$i]})");

            if($resultReceiveFile[$i] == false) {
                $failureList[$receiveIndex++] = $pReceiveFiles[$i];
                $failureCount++;
            }
        }

        if($failureCount > 0 && $failureCount == count($pReceiveFiles)) {
            return [false, l("failure_work_receive_file")];
        }

        //todo diğer yerlrdede aynı kullanıma bak
        if($failureCount > 0) {
            return [false, l("failure_work_receive_file_semi"), $failureList];
        }

        return [true, l("success_work_receive")];
    }

    //todo sorgu hatalı burda kaldın
    public static function Receive($pReceiveId){
        return DB::Select("SELECT * FROM work_receive INNER JOIN work_receive_file USING(work_receive_id) WHERE work_receive_id = {$pReceiveId}");
    }

    public static function Receives($pWorkId, $pUserId = -1){
        return DB::Select("SELECT * FROM work_receive INNER JOIN work_receive_file USING(work_receive_id) AND USING() WHERE work_id = {$pWorkId}".($pUserId == -1?"":" AND user_id = {$pUserId}"));
    }
}
